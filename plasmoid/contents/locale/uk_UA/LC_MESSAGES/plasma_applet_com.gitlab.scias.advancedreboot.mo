��    '      T  5   �      `  &   a     �  "   �     �     �  !   �      
  	   +     5     K     h     z     �     �     �  ]   �               .     J  C   j     �     �     �     �     �            :   ,     g  �   u  8     8   K  7   �     �     �     �  "   �  E    Y   ^	  3   �	  9   �	  #   &
  T   J
  P   �
  f   �
     W  &   n  =   �  4   �  %     !   .  )   P     z  �   �     G  A   T  @   �  C   �  �     B   �  +   �  &   (  '   O  (   w      �  )   �  ~   �     j  ;  y  T   �  U   
  �   `  E   �  #   5     Y  /   t        !   $              #                          
                                 	                          "                                   &                         '   %    </b> has been set for the next reboot. Advanced reboot Behavior upon selecting an entry : Bootloader Menu Can reboot to a custom entry Can reboot to the Bootloader Menu Can reboot to the Firmware Setup Configure Configure this applet Could get the custom entries Debug information Don't reboot Entry management Firmware Setup Ignore In case of issues or missing entries, please ensure that the requirements shown below are met Log Move this entry down the list Move this entry up the list No boot entries could be found. No boot entries could be listed.
Please check this applet settings. Reboot after confirmation Reboot immediately Reboot to... Reset configuration Reset settings? Reset this applet Retry as root Root access is required to get the full boot entries list. The entry <b> This applet cannot work on this system.
Please check that the system is booted in UEFI mode and that systemd, systemd-boot are used and configured properly. This plasmoid's configuration and state have been reset. This will reset this plasmoid's configuration and state. Toggle and rearrange entries displayed in the main view Toggle display of this entry View log bootctl is present systemd is at version 251 or above Project-Id-Version: advancedreboot
Report-Msgid-Bugs-To: https://www.gitlab.com/Scias/plasma-advancedreboot
PO-Revision-Date: 2024-06-04 19:03+0300
Last-Translator: Sergiy Golovko <cappelikan@gmail.com>
Language-Team: 
Language: uk_UA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 </b> встановлено для наступного перезавантаження. Розширене перезавантаження Поведінка після вибору запису : Меню завантажувача Можна перезавантажити до спеціального запису Можна перезавантажити в меню завантажувача Можна перезавантажитися до налаштування мікропрограми Налаштувати Налаштуйте цей аплет Можна отримати спеціальні записи Інформація про налагодження Не перезавантажуйте Управління входом Налаштування прошивки Ігнорувати У разі виникнення проблем або відсутності записів переконайтеся, що наведені нижче вимоги виконані Журнал Перемістіть цей запис вниз у списку Перемістіть цей запис угору списку Записи для завантаження не знайдено. Не вдалося відобразити записи для завантаження.
Перевірте налаштування цього аплету. Перезавантажте після підтвердження Перезавантажте негайно Перезавантажити до... Скинути конфігурацію Скинути налаштування? Скинути цей аплет Повторіть спробу як root Щоб отримати повний список записів завантаження, потрібен доступ root. Запис <b> Цей аплет не може працювати в цій системі.
Будь ласка, переконайтеся, що система завантажується в режимі UEFI і що systemd, systemd-boot використовуються та налаштовані належним чином. Конфігурацію та стан цього плазмоїда скинуто. Це скине конфігурацію та стан цього плазмоїда. Перемикайте та змінюйте порядок записів, що відображаються в головному вікні Перемкнути відображення цього запису Переглянути журнал bootctl присутній systemd має версію 251 або вище 